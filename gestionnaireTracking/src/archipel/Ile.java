package archipel;

import java.util.ArrayList;

public class Ile {
    private static int qteIle;
    private int id;
    private String nom;
    private ArrayList<Plage> plages;
    
    public Ile(String nom){
        plages = new ArrayList<>();
        id = qteIle++;
        this.nom = nom;
    }
    
    public ArrayList<Plage> getPlage(){
        return plages;
    }
    
    public void attribuerPlage(Plage plage){
        plages.add(plage);
    }
    
    @Override
    public String toString(){
        return this.nom;
    }
}
