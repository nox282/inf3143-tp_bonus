package archipel;

import java.util.ArrayList;
import logement.Case;

public class Plage {
    private static int qtePlage;
    private int id;
    private ArrayList<Case> logements;
    private Ile ile;
    
    public Plage(Ile ile){
        logements = new ArrayList<>();
        id = qtePlage++;
        this.ile = ile;
    }
    
    public void attribuerCase(Case cabane){
        logements.add(cabane);
    }
    
    public ArrayList<Case> getCases(){
        return logements;
    }
    
    public void setIle(Ile ile){
        this.ile = ile;
    }
    
    public Ile getIle(){
        return ile;
    }
    
    @Override
    public String toString(){
        return ile.toString()+" plage : "+id;
    }
}
