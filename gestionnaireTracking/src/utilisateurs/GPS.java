package utilisateurs;

import java.util.ArrayList;
import java.util.Iterator;
import utils.Position;

public class GPS {
    private String nSerie;
    private Vacancier utilisateur;
    private ArrayList<Position> deplacements;
    
    public GPS(int serie){
        deplacements = new ArrayList<>();
        if(serie < 10)
            this.nSerie = "TP000"+serie;
        else if (serie < 100)
            this.nSerie = "TP00"+serie;
        else if (serie < 1000)
            this.nSerie = "TP0"+serie;
        else
            this.nSerie = "TP"+serie;
    }
    
    public void enregistrer(Position position){
        deplacements.add(position);
    }
    
    public void attribuer(Vacancier v1){
        this.utilisateur = v1;
    }
    
    public boolean estLibre(){
        return this.utilisateur == null;
    }
    
    public void liberer() {
        this.utilisateur = null;
        deplacements.clear();
    }
    
    public String retracer(){
        StringBuilder ret = new StringBuilder();
        for (Iterator<Position> iterator = deplacements.iterator(); iterator.hasNext();) {
            Position next = iterator.next();
            ret.append(next.toString()+";   ");
        }
        return ret.toString();
    }
    
    public Vacancier getUtilisateur(){
        return utilisateur;
    }
    
}