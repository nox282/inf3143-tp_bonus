package utilisateurs;

import archipel.Plage;
import execution.BoiteGPS;
import execution.ListeLogement;
import logement.impl.Lit;
import utils.Mois;
import utils.Position;

public class Vacancier {
    private int nAssurance;
    private String nom;
    private String prenom;
    private char sexe;
    private GPS gps;
    private Lit logement;
    
    public Vacancier(int ass, String nom, String prenom, char sexe){
        this.nAssurance = ass;
        this.nom = nom;
        this.prenom = prenom;
        this.sexe = sexe;
    }
    
    public Position bouger(Plage plage){
        Position pos = Position.refresh(plage);
        gps.enregistrer(pos);
        return pos;
    }
    
    public char getSexe(){
        return sexe;
    }
    
    public GPS getGPS(){
        return gps;
    }
    
    public Lit getLogement(){
        return logement;
    }
    
    public String getNom(){
        return nom+" "+prenom;
    }
    
    public void arriver(Mois mois){
        gps = BoiteGPS.fouillerBoite();
        gps.attribuer(this);
        logement = ListeLogement.trouverLogement(this, mois);
    }
    
    public void depart(){
        logement.liberer();
        gps.liberer();
    }
}
