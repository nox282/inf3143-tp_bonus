package logement;

import archipel.Plage;
import logement.impl.Lit;
import utilisateurs.Vacancier;
import utils.Mois;

public class Case {
    private static int qteCase;
    private int id;
    protected Lit lits[];
    protected int nbreOccupant;
    protected Plage plage;
    
    public Case(Plage plage){
        this.id = qteCase++;
        this.plage = plage;
        lits = new Lit[4];
        for (int i = 0; i < lits.length; i++) {
            lits[i] = new Lit();
        }
    }
    
    public Lit attribuerChambre(Vacancier nouvelOccupant, Mois mois){
        Lit ret = null;
        if (!estPleine()) {
            for (int i = 0; i < lits.length; i++)
                if(lits[i].estLibre()){
                    lits[i].reserver(nouvelOccupant, mois);
                    ret = lits[i];
                }
        }
        return ret;
    }
    
    public boolean estPleine(){
        boolean ret = false;
        for (int i = 0; i < lits.length; i++)
            ret = lits[i].estLibre();
        return !ret;
    }
    
    public boolean estVide(){
        int count = 0;
        for (int i = 0; i < lits.length; i++)
            if(lits[i].estLibre())
                count++;
        return count == lits.length;
    }
    
    public Plage getPlage(){
        return plage;
    }
    
    public Lit[] getLits(){
        return lits;
    }
}
