package logement;

import archipel.Plage;
import logement.impl.CaseMixte;
import logement.impl.CaseNonMixte;

public class CaseFactory {
    public static Case constructCaseMixte(Plage plage){
        CaseMixte ret = new CaseMixte(plage);
        plage.attribuerCase(ret);
        for (int i = 0; i < ret.lits.length; i++) {
            ret.lits[i].attribuerCabane(ret);
        }
        return ret;
    }
    
    public static Case constructCaseNonMixte(Plage plage){
        CaseNonMixte ret = new CaseNonMixte(plage);
        plage.attribuerCase(ret);
        for (int i = 0; i < ret.lits.length; i++) {
            ret.lits[i].attribuerCabane(ret);
        }
        return ret;
    }
    
    private CaseFactory(){}
}
