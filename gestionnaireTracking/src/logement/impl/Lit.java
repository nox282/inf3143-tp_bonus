package logement.impl;

import logement.Case;
import utilisateurs.Vacancier;
import utils.Mois;

public class Lit {
    private Vacancier occupant;
    private Mois reservation;
    private Case cabane;
    
    public void attribuerCabane(Case cabane){
        this.cabane = cabane;
    }        
           
    //TODO : return type so it signals when reservation isn't completed
    public void reserver(Vacancier nouvelOccupant, Mois mois) {
        if (cabane instanceof CaseNonMixte){
            if(CaseNonMixte.verifierSexe(nouvelOccupant, cabane))
                donnerChambre(nouvelOccupant, mois);
        } else
            donnerChambre(nouvelOccupant, mois);
    }
    
    private void donnerChambre(Vacancier nouvelOccupant, Mois mois){
        occupant = nouvelOccupant;
        reservation = mois;
    }
    
    public void liberer(){
        occupant = null;
        reservation = null;
    }
    
    public Vacancier getOccupant(){
        return occupant;
    }
    
    public boolean estLibre(){
        return occupant == null;
    }
    
    public Case getCase(){
        return cabane;
    }
}
