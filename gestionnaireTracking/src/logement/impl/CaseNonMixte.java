package logement.impl;

import archipel.Plage;
import logement.Case;
import utilisateurs.Vacancier;

public class CaseNonMixte extends Case{
    
    public CaseNonMixte(Plage plage) {
        super(plage);
    }
    
    public static boolean verifierSexe(Vacancier v1, Case cabane){
        Lit[] lits = cabane.getLits();
        char sexe;
        if(cabane.estVide())
            return true;
        else{
            sexe = lits[0].getOccupant().getSexe();
            return sexe == v1.getSexe();
        }
    }
}
