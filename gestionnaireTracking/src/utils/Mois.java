package utils;

public class Mois {
    private int mois;
    private int annee;
    private int id;
    
    private Mois(int mois, int annee){
        this.mois = mois;
        this.annee = annee;
        this.id = (mois*10000)+annee;
    }
    
    public static Mois getMois(int mois, int annee){
        return new Mois(mois, annee);
    }
}
