package utils;

import archipel.Ile;
import archipel.Plage;

public class Position {
    private Ile ile;
    private Plage plage;
    
    private Position(Plage plage){
        this.plage = plage;
        this.ile = plage.getIle();
    }
    
    public static Position refresh(Plage plage){
        return new Position(plage);
    }
    
    @Override
    public String toString(){
        return plage.toString();
    }
    
}
