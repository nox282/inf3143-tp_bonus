package execution;

import archipel.Ile;
import archipel.Plage;
import java.util.ArrayList;
import java.util.Iterator;
import logement.Case;
import logement.CaseFactory;
import logement.impl.Lit;
import utilisateurs.Vacancier;
import utils.Mois;

public class ListeLogement {
    private static ArrayList<Ile> archipel;
    
    /**
     * Invoque les forces obscures et fait apparaitre une archipel avec des plages et des cases deja construite dessus
     */
    public static void initierLogement(){
        archipel = new ArrayList<>();
        Ile ile;
        for (int i = 0; i < 10; i++) {
            ile = new Ile(nomAleatoire());
            archipel.add(ile);
        }
        initierPlages();
    }
    
    public static Lit trouverLogement(Vacancier v1, Mois mois){
        Lit ret = null;
        for (Iterator<Ile> iterator = archipel.iterator(); iterator.hasNext();) {
            Ile next = iterator.next();
            for (Iterator<Plage> iterator1 = next.getPlage().iterator(); iterator1.hasNext();) {
                Plage next1 = iterator1.next();
                for (Iterator<Case> iterator2 = next1.getCases().iterator(); iterator2.hasNext();) {
                    Case next2 = iterator2.next();
                    ret = next2.attribuerChambre(v1, mois);
                    if(ret != null)
                        return ret;
                }
            }
        }
        return ret;
    }
    
    private static void initierPlages(){
        Plage plage;
        int nbrePlage;
        int nbreCase;
        
        for (Iterator<Ile> iterator = archipel.iterator(); iterator.hasNext();){
            Ile next = iterator.next();
            nbrePlage = nombreAleatoire(1, 10);
            for (int i = 0; i < nbrePlage; i++){
                plage = new Plage(next);
                next.attribuerPlage(plage);
                nbreCase = nombreAleatoire(5, 15);
            
                for (int j = 0; j < nbreCase; j++) {
                    if(j < nbreCase/2)
                        CaseFactory.constructCaseMixte(plage);
                    else
                        CaseFactory.constructCaseNonMixte(plage);
                }
            }
        }
    }
    
    private static String nomAleatoire(){
        int longueur = nombreAleatoire(4, 8);
        StringBuilder ret = new StringBuilder();
        ret.append((char)nombreAleatoire(65, 90));         //Premiere lettre masjuscule
        for (int i = 0; i < (longueur-1); i++)
            ret.append((char)nombreAleatoire(97, 122));   //Le reste en minuscule
        return ret.toString();
    }
    
    /**
     * from http://stackoverflow.com/questions/7961788/math-random-explained
     */
    private static int nombreAleatoire(int min, int max){
        int range = (max - min) + 1;
        return (int)(Math.random()*range) + min;
    }
    
    public static Plage plageAleatoire(){
        int ileId = nombreAleatoire(0, 9);
        int plageId = nombreAleatoire(0, 14);
        
        Ile ile = archipel.get(ileId);
        while(plageId >= ile.getPlage().size())
            plageId = nombreAleatoire(0, 14);
        
        return ile.getPlage().get(plageId);
    }
}