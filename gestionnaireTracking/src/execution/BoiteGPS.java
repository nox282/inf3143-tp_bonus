package execution;

import java.util.ArrayList;
import java.util.Iterator;
import utilisateurs.GPS;

public class BoiteGPS {
    private static ArrayList<GPS>boite;
    
    /**
     * Paremetre 1000 GPS a donner aux Vacanciers.
     */
    public static void initierBoite(){
        boite = new ArrayList<>();
        GPS gps;
        for (int i = 0; i < 1000; i++) {
            gps = new GPS(i+1);
            boite.add(gps);
        }
    }
    
    /**
     * Fouille la boite a GPS
     * @return le premier GPS libre.
     */
    public static GPS fouillerBoite(){
        for (Iterator<GPS> iterator = boite.iterator(); iterator.hasNext();) {
            GPS next = iterator.next();
            if(next.estLibre())
                return next;
        }
        return null;
    }
}