package execution;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import utilisateurs.GPS;
import utilisateurs.Vacancier;
import utils.Mois;

public class Main {

    public static void main(String[] args) {
        BoiteGPS.initierBoite();
        ListeLogement.initierLogement();
        Mois  mois = aujourdhui();
        ArrayList<Vacancier> touristes = genererTouriste();
        
        //Les vacanciers arrivent sur l'ile et loue chacun une chambre.
        for (Iterator<Vacancier> iterator = touristes.iterator(); iterator.hasNext();) {
            Vacancier next = iterator.next();
            next.arriver(mois);
        }
        
        //Ils se balandent pendant la journee.
        for (Iterator<Vacancier> iterator = touristes.iterator(); iterator.hasNext();) {
            Vacancier next = iterator.next();
            next.bouger(ListeLogement.plageAleatoire());
        }
        
        //La nuit tombe ils rentrent dans leur cases.
        for (Iterator<Vacancier> iterator = touristes.iterator(); iterator.hasNext();) {
            Vacancier next = iterator.next();
            next.bouger(next.getLogement().getCase().getPlage());
        }
        
        //Pendant la nuit, on va logger les trajets qu'ils ont emprunte
        ArrayList<GPS> listGPS = new ArrayList<>();
        for (Iterator<Vacancier> iterator = touristes.iterator(); iterator.hasNext();) {
            Vacancier next = iterator.next();
            listGPS.add(next.getGPS());
        }
        logGPS(listGPS);
        
        // Leur sejour est deja finit
        for (Iterator<Vacancier> iterator = touristes.iterator(); iterator.hasNext();) {
            Vacancier next = iterator.next();
            next.depart();
        }
    }
    
    private static ArrayList<Vacancier> genererTouriste(){
        ArrayList<Vacancier> touristes = new ArrayList<>();
        Vacancier jeanAlfred = new Vacancier(0, "Jean", "Alfred", 'h');
        touristes.add(jeanAlfred);
        Vacancier marieFrancoise = new Vacancier(0, "Marie", "Francoise", 'f');
        touristes.add(marieFrancoise);
        Vacancier philibertComment = new Vacancier(0, "Philibert", "Comment", 'h');
        touristes.add(philibertComment);
        Vacancier jeanEdouard = new Vacancier(0, "Jean", "Edouard", 'h');
        touristes.add(jeanEdouard);
        Vacancier bilyCeasar  = new Vacancier(0, "Bily", "Ceasar", 'h');
        touristes.add(bilyCeasar);
        Vacancier axelleRed = new Vacancier(0, "axelle", "Red", 'f');
        touristes.add(axelleRed);
        Vacancier renaudBrey = new Vacancier(0, "Renaud", "Brey", 'h');
        touristes.add(renaudBrey);
        Vacancier amandinePotiron = new Vacancier(0, "Amandine", "Potiron", 'f');
        touristes.add(amandinePotiron);
        Vacancier javaCornichon = new Vacancier(0, "Java", "Cornichon", 'h');
        touristes.add(javaCornichon);
        return touristes;
    }
    
    private static Mois aujourdhui(){
        Calendar cal = Calendar.getInstance();
        return Mois.getMois(cal.get(Calendar.MONTH)+1, cal.get(Calendar.YEAR)+1);
    }
    
    private static void logGPS(ArrayList<GPS> list){
        for (Iterator<GPS> iterator = list.iterator(); iterator.hasNext();) {
            GPS next = iterator.next();
            System.out.println("Utilisateur "+next.getUtilisateur().getNom());
            System.out.println("\t"+next.retracer());
        }
    }
    
}
